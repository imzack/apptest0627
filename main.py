
import pytest
import os,time
import multiprocessing,subprocess


def get_connect_devices():
    """
    获取已经成功连接的设备
    :return: list
    """
    devices = []
    port = 4723

    proc = subprocess.Popen('adb devices', stdout=subprocess.PIPE,shell=True)

    for line in proc.stdout.readlines():
        # print(type(line),line)
        # 将字节类型转换为字符串
        linestr = line.decode(encoding='utf8')
        if '\tdevice' in linestr:
            # 字符串分割 提取 deviceid值
            device_id = linestr.strip().split('\tdevice')[0]
            devices.append((device_id,port))
            port += 2 # 端口递增2

    return devices

def run(device):
    # 进程启动之后设置变量
    os.environ['udid'] = str(device[0])
    os.environ['port'] = str(device[1])
    report_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'reports')
    if not os.path.exists(report_dir):
        os.mkdir(report_dir)

    report = str(device[1])+time.strftime('%Y_%m_%d_%H_%M_%S')

    reportfile = os.path.join(report_dir, report + '.html')

    pytest.main(['testcases', '-s', '-v', f'--html={reportfile}'])


if __name__ == '__main__':
    devices = get_connect_devices()
    process = []

    for device in devices:
        # 创建进程
        p = multiprocessing.Process(target=run,args=(device,))
        p.start()
        process.append(p)

    for proc in process:
        proc.join()