import os
from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet
class FileHandler:

    def __init__(self):
        self.__excel_path = os.path.join(os.path.dirname( os.path.abspath(__file__)),'../testdata/data.xlsx')

    def get_data_by_sheet(self,sheetName):
        wb = load_workbook(self.__excel_path)
        if sheetName not in wb.sheetnames:
            raise Exception(f"请检查sheet的名字，{sheetName}不在 {wb.sheetnames}中")
        sheet:Worksheet = wb[sheetName]
        sheet_data = []
        for row in sheet.iter_rows(min_row=2,max_row=sheet.max_row,
                                   min_col=1,max_col=sheet.max_column,values_only=True):
            sheet_data.append(row)

        return sheet_data


