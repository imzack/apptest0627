from pom.homepage import HomePage
from pom.loginpage import LoginPage

def test_create_topic(driver):
    loginpage = LoginPage(driver)
    # 用户登录成功
    loginpage.with_token_login('3099e7fb-585b-45ba-8ef5-ace10f360d4b')

    # 首页打开
    hp = HomePage(driver)
    # 进入创建话题页面
    create_page = hp.go_create_topic()

    create_page.new_topic(tab='分享',title='123',content='012')
    result = create_page.result_text
    assert result == "标题要求10字以上"