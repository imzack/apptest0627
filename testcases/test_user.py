from pom.loginpage import LoginPage
# 使用conftest.py 中定义的 driver
def test_login(driver):
    # 打开登录页面
    loginpage = LoginPage(driver)
    # 使用token进行登录
    loginpage.with_token_login('tokenxxxxxxx')

    # 登录成功， 验证toast的文本值为登录成功
    result = loginpage.result_text
    assert result == "登录成功"

