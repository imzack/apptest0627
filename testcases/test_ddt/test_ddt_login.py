
import pytest

from pom.loginpage import LoginPage
from utils.file_handler import FileHandler

fl = FileHandler()
# 从Excel文件中获取数据
data = fl.get_data_by_sheet('用户登录')

class TestLogin:

    @pytest.mark.parametrize('token,status,expect_val',data)
    def test_login(self,driver,token,status,expect_val):
        # 打开登录页面
        loginpage = LoginPage(driver)
        # 使用token进行登录
        loginpage.with_token_login(token)
        if status == '成功':
            # 登录成功， 验证toast的文本值为登录成功
            result = loginpage.result_text
            assert result == expect_val
        if status == "失败":
            result = loginpage.with_token_failed_text
            assert result == expect_val
