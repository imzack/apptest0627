from appium.webdriver.webdriver import WebDriver
import time
from pom.basepage import BasePage


class LoginPage(BasePage):

    def __init__(self,driver:WebDriver):
        super(LoginPage, self).__init__(driver)

        # 保证页面是在登录页面上
        # 判断当前driver的状态
        current_activity = self.driver.current_activity
        if ".ui.activity.LoginActivity" in current_activity:
            pass
        else:
            # 打开登录页面
            self.__go_login_page()

        # 初始化类的时候 打开这个页面
        # self.driver.start_activity(app_package="org.cnodejs.android.md",
        #                            app_activity='.ui.activity.LoginActivity')

    def with_token_login(self,token):
        """
        使用token的方式进行登录
        :param token:
        :return:
        """
        self.driver.find_element_by_id('org.cnodejs.android.md:id/edt_access_token').send_keys(token)
        loginbtn = self.driver.find_element_by_android_uiautomator(
            'text("登录").resourceId("org.cnodejs.android.md:id/btn_login")')
        loginbtn.click()

    def __go_login_page(self):
        """
        导航到loginpage
        :return:
        """
        # 清空app的状态 如果已经登录，去掉登录状态
        self.driver.reset()
        # 打开首页
        self.driver.start_activity(app_package='org.cnodejs.android.md',app_activity='.ui.activity.LaunchActivity')

        toggle_btn = self.driver.find_element_by_android_uiautomator('resourceId("org.cnodejs.android.md:id/toolbar")'
                                                                '.childSelector(new UiSelector().className("android.widget.ImageButton"))')
        toggle_btn.click()
        time.sleep(1)

        # 点击头像
        avatar = self.driver.find_element_by_android_uiautomator('text("点击头像登录")'
                                                            '.resourceId("org.cnodejs.android.md:id/tv_login_name")')
        avatar.click()

    @property
    def with_token_failed_text(self):
        # 1. 截图
        ele = self.driver.find_element_by_id('org.cnodejs.android.md:id/edt_access_token')
        png = ele.screenshot_as_base64
        # 2. TODO 调用 ocr 图片识别 将图片中文字识别出来

        return ""

    def with_code_login(self):
        pass

    def with_github_login(self):
        pass