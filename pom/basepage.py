from appium.webdriver.webdriver import  WebDriver
import time

from selenium.common.exceptions import NoSuchElementException


class BasePage:
    def __init__(self,driver:WebDriver):
        self.driver = driver


    @property
    def result_text(self):
        """
        获取Toast的文本值
        :return:
        """
        try:
            toast = self.driver.find_element_by_xpath('//android.widget.Toast')
            return  toast.text
        except NoSuchElementException:
            return  "找不到这个元素，请检查自己的自动化代码"