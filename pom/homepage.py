"""
首页
"""
from pom.basepage import BasePage
from pom.createtopicpage import CreateTopicPage


class HomePage(BasePage):

    def __init__(self,driver):
        super(HomePage, self).__init__(driver)

        if '.ui.activity.MainActivity' in self.driver.current_activity:
            pass
        else:
            self.__go_home_page()
        # # 打开首页
        # self.driver.start_activity(app_package='org.cnodejs.android.md',
        #                            app_activity='.ui.activity.MainActivity')
    def __go_home_page(self):
        self.driver.start_activity(app_package='org.cnodejs.android.md', app_activity='.ui.activity.LaunchActivity')

    def go_create_topic(self):
        # 判断是否已经到达创建话题页面
        while not '.ui.activity.CreateTopicActivity' in self.driver.current_activity:
            # 再重新点击一下
            create_btn = self.driver.find_element_by_android_uiautomator(
                '.resourceId("org.cnodejs.android.md:id/fab_create_topic")')
            create_btn.click()

        return CreateTopicPage(self.driver)