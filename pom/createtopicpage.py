"""
发帖页面
"""
from pom.basepage import BasePage


class CreateTopicPage(BasePage):

    def new_topic(self,tab,title,content):
        spinner = self.driver.find_element_by_android_uiautomator('.resourceId("org.cnodejs.android.md:id/spn_tab")')
        spinner.click()
        tab_selcotor = f'.resourceId("android:id/text1").text("{tab}")'
        self.driver.find_element_by_android_uiautomator(tab_selcotor).click()

        title_content = self.driver.find_element_by_android_uiautomator('resourceId("org.cnodejs.android.md:id/edt_title")')
        title_content.send_keys(title)

        content_area = self.driver.find_element_by_android_uiautomator('resourceId("org.cnodejs.android.md:id/edt_content")')
        content_area.send_keys(content)

        send_btn = self.driver.find_element_by_android_uiautomator('resourceId("org.cnodejs.android.md:id/action_send")')
        send_btn.click()


